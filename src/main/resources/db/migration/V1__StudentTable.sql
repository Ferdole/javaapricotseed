CREATE TABLE student (
    id UUID NOT NULL PRIMARY KEY,
    name varchar(100) NOT NULL,
    age int NOT NULL,
    email varchar(100) UNIQUE
);