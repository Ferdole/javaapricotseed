package com.example.Student.controller;

import com.example.Student.exception.StudentNotFoundException;
import com.example.Student.model.Student;
import com.example.Student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/home")
public class StudentController  {

    @Autowired
    private StudentService studentService;

    @PostMapping(path = "/insertStudent")
    public int insertStudent(@Valid @NonNull @RequestBody Student student){
        return studentService.insertStudent(student);
    }

    @GetMapping
    public Collection<Student> getAllStudent(){
        return studentService.getAllStudents();
    }

    @GetMapping(path = "/student/{id}")
    public Student selectStudentById(@PathVariable("id") UUID id){
        Student student = studentService.selectStudentByID(id);

        if (student == null ){
            System.out.println("no Student Found");
        } else {
            return student;
        }

        return null;
    }

    @PutMapping(path = "/mapUpdate/{id}")
    public int mapUpdate(@PathVariable("id") UUID id,
                         @RequestBody Map<String, Object> payload){
        return studentService.mapUpdate(id, payload);
    }

    @PutMapping(path = "/update/{id}")
    public void update(@PathVariable("id") UUID id,
                       @RequestBody Map<String, String> payload){
        System.out.println(payload);
    }

    @DeleteMapping(path = "/deleteStudent/{id}")
    public int deleteStudentById(@PathVariable("id") UUID id){
        return studentService.deleteStudentById(id);
    }

    @PutMapping(path = "/preparedUpdate/{id}")
    public Integer preparedUpdate(@PathVariable("id") UUID id,
                              @Validated @RequestBody Student student) {
        final int response = studentService.preparedUpdate(id, student);
        if (response == 1)
            return response;
        else
            throw new StudentNotFoundException();
    }

}
