package com.example.Student.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class Student {

    private UUID id;
    @NotNull private String name;
    @NotNull private Integer age;
    @NotNull private String email;

    public Student (){};

    // Student constructor for insert UUID id is generate by DB
    // used for simpleUpdate
//    public Student(
//            @JsonProperty String name,
//            @JsonProperty Integer age,
//            @JsonProperty String email) {
//        this.name = name;
//        this.age = age;
//        this.email = email;
//    }

    // Student constructor for displaying info
    public Student(UUID id, String name, Integer age, String email) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    //    public Student(String name) {
//        this.name = name;
//    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }
}
