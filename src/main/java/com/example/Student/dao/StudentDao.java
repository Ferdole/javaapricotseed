package com.example.Student.dao;

import com.example.Student.model.Student;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface StudentDao {

    int insertSutdent(Student student);

    default int insertStudent(Student student){
        return insertSutdent(student);
    }

    List<Student> getAllStudents();

    Student selectStudentById(UUID id);

    int deleteStudentById(UUID id);

    int mapUpdate(UUID id, Map<String, Object> payload);

    int preparedUpdate(UUID id, Student student);
}
