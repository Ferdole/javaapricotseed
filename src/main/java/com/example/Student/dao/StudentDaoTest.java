package com.example.Student.dao;

import com.example.Student.model.Student;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository("fakeDao")
public class StudentDaoTest {

    private static Map<Integer, Student> students;
//    public static Student dorel = new Student(1, "Dorel", 23, "asd");

    static {
        students = new HashMap<Integer, Student>(){

            {
                put(1, new Student(UUID.randomUUID(), "wasdd", 12, "23@g"));
                put(2, new Student(UUID.randomUUID(), "Dorel", 22, "asd3@g"));
                put(3, new Student(UUID.randomUUID(), "Ion", 32, "23@g"));
            }
        };
    }

    public Collection<Student> getAllStudents(){
        return this.students.values();
    }

    public Student getStudentById(int id){
        return this.students.get(id);
    }

}
