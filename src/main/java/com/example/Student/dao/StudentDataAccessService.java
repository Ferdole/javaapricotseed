package com.example.Student.dao;

import com.example.Student.exception.StudentNotFoundException;
import com.example.Student.model.Student;
import javafx.util.converter.IntegerStringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpStatusCodeException;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class StudentDataAccessService implements StudentDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StudentDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertSutdent(Student student) {
        String sql = "INSERT INTO student (id, name, age, email) VALUES (uuid_generate_v4(), ? , ? , ?)" ;

        jdbcTemplate.update(sql, student.getName(), student.getAge(), student.getEmail());

        return 1;
    }

    @Override
    public List<Student> getAllStudents() {
        final String sql = "SELECT id, name, age, email FROM STUDENT";
        return jdbcTemplate.query(sql, ((resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");
            int age = resultSet.getInt("age");
            String email = resultSet.getString("email");
            return new Student(id, name, age, email);
        }));
    }

    @Override
    public Student selectStudentById(UUID id) {
        final String sql = "SELECT id, name, age, email FROM student WHERE id = ?";

        // get object student from DB and populate fields.
        try {
            Student student = jdbcTemplate.queryForObject(
                    sql,
                    new Object[]{id},
                    (resultSet, i) -> {
                        UUID personId = UUID.fromString(resultSet.getString("id"));
                        String name = resultSet.getString("name");
                        int age = resultSet.getInt("age");
                        String email = resultSet.getString("email");
                        return new Student(personId, name, age, email);
                    });

            return student;
        } catch (EmptyResultDataAccessException e) {
            throw new StudentNotFoundException();
        }
    }

    @Override
    public int deleteStudentById(UUID id) {
        final String sql = "DELETE FROM student where id= ?";
        System.out.println("ID = " + id);
        if (jdbcTemplate.update(sql, id) == 0 ) {
            throw new StudentNotFoundException();
        } else {
            return 1;
        }
    }

    @Override
    public int mapUpdate(UUID id, Map<String, Object> payload){
        sqlStringBuilder sqlString = new sqlStringBuilder();
        String sqlUpdate =  sqlString.getSqlUpdateFromMap(id, payload);
        System.out.println(sqlUpdate);
        jdbcTemplate.execute(sqlUpdate);
        return 1;
    }

    @Override
    public int preparedUpdate(UUID id, Student student) {
        String sql = "UPDATE student set name = ?, age = ?, email = ? WHERE id = ?";
        int response = jdbcTemplate.update(sql, student.getName(), student.getAge(), student.getEmail(), id);
        System.out.println("jdbcTemplate response: " + response);
        if (response == 1)
            return 1;
        else
            throw new StudentNotFoundException();
    }

}
