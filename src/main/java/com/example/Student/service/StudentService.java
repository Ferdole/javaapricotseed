package com.example.Student.service;

import com.example.Student.dao.StudentDao;
import com.example.Student.dao.StudentDaoTest;
import com.example.Student.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class StudentService {

//    @Autowired
//    @Qualifier("fakeDao")
    private final StudentDao studentDao;

    @Autowired
    public StudentService(@Qualifier("postgres") StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public int insertStudent(Student student){
        return studentDao.insertStudent(student);
    }

    public Collection<Student> getAllStudents(){
        return studentDao.getAllStudents();
    }

    public Student selectStudentByID(UUID id){
        return studentDao.selectStudentById(id);
    }

    public int deleteStudentById(UUID id){
        return studentDao.deleteStudentById(id);
    }

    public int mapUpdate(UUID id, Map<String, Object> payload){
        return studentDao.mapUpdate(id, payload);
    }

    public int preparedUpdate(UUID id, Student student){
        return studentDao.preparedUpdate(id, student);
    }
}
